# Assignment description:
# 2.	Write a program to read two lists of names from two input files
#       and then match the names in the two lists using Co-sequential
#       Match based on a single loop. Output the names common to both the
#       lists to an output file (40 points).


# match() takes the two sorted arrays and performs cosequential search on them.
# This allows a single loop to be performed to find all matches. The search will terminate
# as soon as either list reaches their end, as no more possible matches may be found
# at that point.
def match(list1, list2):
    i = 0
    j = 0
    matchlist = []

    while i < len(list1) and j < len(list2):
        # print(list1[i])
        # print(list2[j])
        if list1[i] < list2[j]:
            i += 1
        elif list1[i] > list2[j]:
            j += 1
        else:
            matchlist.append(list1[i])
            i += 1
            j += 1

    print("matchlist is: ")
    print(matchlist)

    return -1


# first function which creates arrays of the two name files.
# the arrays are then sorted, and sent to the match() function.
def create_arrays():
    file1 = open('namelist_1.txt', 'r')
    list1 = []
    nextline = file1.readline()
    while nextline != '':
        # print(nextline)
        temp = nextline.split('\n', 1)
        list1.append(temp[0])
        nextline = file1.readline()
    file1.close()
    list1.sort()
    # print(list1)

    file2 = open('namelist_2.txt', 'r')
    list2 = []
    nextline = file2.readline()
    while nextline != '':
        # print(nextline)
        temp = nextline.split('\n', 1)
        list2.append(temp[0])
        nextline = file2.readline()
    file2.close()
    list2.sort()
    # print(list2)

    match(list1, list2)


create_arrays()
