
# Assignment description:
# 1.	Write a program to implement simple index on primary key for a file of employee records:
#
# a.	Implement add() to ask users to enter information about employee records, including their EID, Name, Age, and Rating,
#       store these information to a fixed-length record file. Create an index file at the same time by using EID as primary
#       key and RRN as reference (30 points).
#
# b.	Implement search ( ) by EID using the index file, and display the record on screen if record found, otherwise, display
#       “Record not found!” (30 points)


# Record format is: EID,Name,Age,Rating.
# EID has a length of 4
# Name has a length of 8, if name is < 8 chars, the remaining space is filled with 1's
# Age has a length of 2 (Sorry centenarians)
# Rating has a length of 2, from 01 - 05
# thus format will look something like 0123Name11112905


def write_to_record(record_string):
    file = open('homework3_record.txt', "a")
    # record_string += '\n'
    file.write('\n')
    file.write(record_string)

    file.close()
    main()
    return -1


def get_eid():
    # Things to implement: Check current index for EID already existing.
    eid_val = input("Input a 4 digit EID number: ")
    if len(eid_val) != 4 or eid_val.isnumeric() is False:
        print("Wrong format.")
        eid_val = ''
        get_eid()
    print(eid_val)
    return eid_val


def get_name():
    name = input(
        "Enter first name. If name is greater than 8 characters, it will be truncated to 8 characters.")
    if len(name) > 8:
        name = name[:8]
    elif len(name) < 8:
        delta = 8 - len(name)
        # print("delta is %d" % delta)
        for i in range(delta):
            name += ' '
        # print(name)
    # print("Your name is: " + name)
    return name


def get_age():
    age_val = input("Input your age: ")
    if len(age_val) != 2 or age_val.isnumeric() is False:
        print("Wrong format.")
        age_val = ''
        get_age()
    print(age_val)
    return age_val


def get_rating():
    rating_range = ['01', '02', '03', '04', '05']

    rating_val = input("Input your rating from 01 - 05: ")
    if len(rating_val) != 2 or rating_val not in rating_range:
        print("Wrong format.")
        rating_val = ''
        get_rating()
    print(rating_val)
    return rating_val


# function which adds a new record via making the correct function calls to gather the right information
# and then concatenate the string, and place within record.
def add():
    record_string = ''
    eid = get_eid()
    name = get_name()
    age = get_age()
    rating = get_rating()
    # print(rating)
    record_string += eid + name + age + rating
    # print(record_string)
    write_to_record(record_string)
    return -1


# Binary search of index which is placed into main memory (array). When a match is found
# the record is pulled up using the RRN via the readlines() function.
# The record is then formatted, and printed to console.
def search():
    record = open('homework3_record.txt', "r")
    index = open('homework3_index.txt', "r")
    index_arr = []
    final_record = ''
    begin = -1
    end = 0
    mid = 0

    search_name = get_name()
    print(search_name)

    for i in index:
        index_arr.append(i)

    end = len(index_arr)
    while end - begin != 1:

        mid = int(((end) + (begin)) / 2)
        # print(mid)
        # print((index_arr[mid])[0:8])

        if ((index_arr[mid])[0:8]) == search_name:
            rrn = int(((index_arr[mid])[8::]))
            print('rrn is %d' % rrn)
            space_count = 0
            for j in record.readlines()[(rrn):(rrn + 1)]:
                final_record += '['
                for k in j:
                    if space_count == 3 or space_count == 11 or space_count == 13:
                        final_record += (k + ', ')
                        # print(k + ', '),
                        space_count += 1
                    elif k == '\n':
                        break
                    else:
                        final_record += k
                        # print(k),
                        space_count += 1
            final_record += ']'
            print("Record found: ")
            print(final_record)
            print()
            main()
        elif ((index_arr[mid])[0:8]) > search_name:
            end = mid

        else:
            begin = mid

        # print('begin =  %d' % begin)
        # print('end = %d' % end)
    print("No record found.")
    print()
    main()


# function to create an up to date index whenever it is called.
def create_index():
    record = open('homework3_record.txt', "r")
    index = open('homework3_index.txt', "w")
    index_arr = []

    nextline = record.readline()
    rrn = 0
    while nextline != '':
        name = nextline[4:12]
        # print(name)
        # print(rrn)
        index_tup = (name, rrn)
        index_arr.append(index_tup)
        nextline = record.readline()
        rrn += 1
    index_arr.sort()
    for i in index_arr:
        for j in i:
            index.write(str(j))
        index.write('\n')

    record.close()
    index.close()


# Main method to choose between adding to, or searching from a record.
def main():
    choice = input(
        "Type a letter: 'a': add, 'b': search, and anything else to exit. ")

    if choice == 'a':
        add()
    elif choice == 'b':
        create_index()
        search()
    else:
        exit()
    return -1


main()
