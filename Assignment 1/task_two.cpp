#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>

/* Assignment description:


Write a program to read a series of names, one per line from standard input, and write out these names spelled in reverse order to standard output, and do the following: (10 points bonus if you can provide solution using more than one programming language)
a.	Input a series of names that are typed in from the keyboard and write them out, reversed, to a file called file1. (20 points)
b.	Read the names in from file1; then write them out, re-reversed, to a file called file2. (20 points)
c.	Read the names in from file2, reverse them again, and then sort the resulting list of reversed words and write out these names to standard output. (30 points)
*/

using namespace std;
string reverseName(string name);
void firstfile();
void secondfile();
void finalReadout();

main()
{
    firstfile();
    secondfile();
    finalReadout();
    return 0;
}

void firstfile()
{
    string file;
    string nextLine;
    bool endTrigger = true;
    int top_pos = 0;

    fstream outfile;
    outfile.open("file1.txt", ios::out);

    cout << "input names to be place in a file \n";
    cout << "Press 0 when done: ";

    while (1)
    {
        getline(cin, nextLine);
        nextLine = reverseName(nextLine);

        if (nextLine == "0")
        {
            break;
        }
        else
        {
            /*  Checks if beginning of file. If not,
                it first inserts a newline. this way each
                name is on it's own line, but there is
                no final '\n' at end of file. */
            if (outfile.tellp() == top_pos)
            {
            }
            else
            {
                outfile << "\n";
            }
            outfile << nextLine;
        }
    }
    outfile.close();
}

void secondfile()
{
    char readChar;
    string readline;
    fstream newfile;
    fstream readfile;

    newfile.open("file2.txt", ios::out);
    readfile.open("file1.txt", ios::in);

    if (readfile.is_open())
    {
        readfile >> noskipws;
        while (readfile >> readChar)
        {

            if (readChar == '\n')
            {
                newfile << reverseName(readline);
                newfile << '\n';
                readline.clear();
            }
            else
            {
                readline += readChar;
                // cout << readline;
            }
        }
    }
    newfile << reverseName(readline);

    newfile.close();
    readfile.close();
}

void finalReadout()
{
    char curChar;
    string readout;
    fstream readfile;
    vector<string> names;

    readfile.open("file2.txt", ios::in);

    if (readfile.is_open())
    {
        readfile >> noskipws;
        while (readfile >> curChar)
        {
            if (curChar == '\n')
            {
                names.push_back(reverseName(readout));
                readout.clear();
            }
            else
            {
                readout += curChar;
            }
        }
    }
    names.push_back(reverseName(readout));

    sort(names.begin(), names.end());

    cout << "the final vector reads as: \n";
    for (string i : names)
    {
        cout << i << '\n';
    }
    readfile.close();
};

string reverseName(string name)
{
    string reversed;
    int len = name.length();
    for (int i = len - 1; i >= 0; i--)
    {
        reversed += name[i];
    }
    return reversed;
}