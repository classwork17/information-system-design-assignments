import os.path


# Assignment description:
# Write a program to search sequentially through a file for a record with a particular key, make sure to include the following steps:
# 1.	Ask user “Enter name of the file to search”
# 2.	Open the file
# 3.	Ask user “Enter last name”
# 4.	Ask user “Enter first name”
# 5.	Use the entered names (last name, first name) as search key to sequentially search through the file
# 6.	If record found, print the records
# 7.	If record not found, print “record not found”.


# This function searches one line at a time to find the correct record.
# It splits each newline into an array, separated by the delimiter character '|'
# At which point it does equality checks on both the first and last name.
def seq_search(filename):
    file = open(filename, "r")

    lastname = input("Enter a last name to search: ")
    firstname = input("Enter a first name to search: ")

    nextline = file.readline()
    while nextline != '':
        nextarray = nextline.split("|")

        if lastname == nextarray[0] and firstname == nextarray[1]:
            print("Record found:")
            print(nextline)

            quit()
        else:
            nextline = file.readline()

    print("Record not found.")


# A binary search which first sorts the file contents
# and then compares the last name and first name of each record
# against the 'key' supplied by the user.
def bin_search(filename):
    file = open(filename, "r")

    nextline = file.readline()
    sortRec = []
    while nextline != '':
        sortRec += [nextline.split('|')]
        nextline = file.readline()

    sortRec.sort()

    begin = 0
    end = len(sortRec)

    lastname = input("Enter a last name to search: ")
    firstname = input("Enter a first name to search: ")
    key = [lastname, firstname]

    # The actual binary search algorithm which
    # changes the start and end points of the files
    # to be searched, based on if the record is < or > than the key
    while end - begin != 1:
        rec = ''
        midpoint = int(((begin) + (end)) / 2)
        # print("midpoint is: " + str(midpoint))
        # print("Begin is: " + str(begin))
        # print("end is: " + str(end))
        if sortRec[midpoint][0] == key[0]:
            if sortRec[midpoint][1] == key[1]:
                print("File Found: ")
                # small loop to reconstruct the record structure
                for i in sortRec[midpoint]:
                    rec += (i + '|')
                print(rec)
                quit()
            else:
                if sortRec[midpoint][1] < key[1]:
                    begin = midpoint
                else:
                    end = midpoint
        else:
            if sortRec[midpoint][0] < key[0]:
                begin = midpoint
            else:
                end = midpoint

    print("File not found.")


# Small function for the user to decide sequential, or binary search.
def select_search(filename):
    # file = open(filename, "r")

    search_select = int(input("Enter 1 for sequential search, 2 for binary: "))

    print(search_select)
    if search_select == 1:
        seq_search(filename)
    else:
        bin_search(filename)
    return -1


# function to choose the file needed to be opened.
def file_pick():
    filename = input(
        "Enter a name of the file to search (such as: 'example.txt'): ")

    print(filename)

    if not os.path.isfile(filename):
        print("no file by that name found.")
        file_pick()
    else:
        select_search(filename)


file_pick()
